import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class gestionefile {
	
	public void gestionefile(String[] args) {
		
	}

	public static void main(String[] args)
		throws Exception
	{
		// TODO Auto-generated method stub
		
		gestionefile myFile = new gestionefile();
		myFile.creaFile(args[0]);
		//myFile.leggiFile(args[0]);
	}
	
	private File creaFile(String filepath)
		throws Exception
	{
		File filetowrite;
		if (filepath != null) {
			filetowrite = new File(filepath);
		} else {
			filetowrite = new File("C:\\miofile.txt" );
		}
		
		if ( ! filetowrite.exists()) {
			filetowrite.createNewFile();
			System.out.println("File creato!");
		} else {
			System.out.println("File gi� esistente!");
		}
		
		return filetowrite;
	}
	
	public void leggiFile(String filepath)
			throws Exception
	{
        Path path = Paths.get(filepath);
        String read = Files.readAllLines(path).get(0);

        System.out.println(read);
	}
}
