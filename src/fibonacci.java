/*
 * To print first 30th fibonacci sequence
 * */
public class fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//To initialize integer array of 30 values
		Integer FibonAr[] = new Integer [30];
		//To create integer work variable helpful to fill array
		Integer assign;
		
		//Set first array value to 1
		FibonAr[0] = new Integer (1);
		//Set second array value to 1
		FibonAr[1] = new Integer (1);
		
		//Print first and second array value
		System.out.print(FibonAr[0].toString() + " " + FibonAr[1].toString() + " " );
		
		//Start cycle from third position until array length
		for (int idx = 2; idx < FibonAr.length; idx ++) {
			
			//To calculate current value
			assign = new Integer(FibonAr[idx - 1] + FibonAr[idx - 2]) ;
			//
			FibonAr[idx] = new Integer (assign);
			//Print current fibonacci value
			System.out.print(FibonAr[idx].toString() + " ");
		}
	}
}
