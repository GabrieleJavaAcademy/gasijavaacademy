import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class FileExamples {

    private static final Logger LOGGER = Logger.getLogger( FileExamples.class.getName() );

    public static void main(String[] args) throws IOException {
        FileExamples fe = new FileExamples();

        fe.createFile("test.txt");
        LOGGER.info("fe.createFile(\"test.txt\")");
        fe.readFile("test.txt");
        LOGGER.info("fe.readFile(\"test.txt\")");
        LOGGER.warning("debug");
    }


    public void createFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        byte[] strToBytes = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa".getBytes();

        Files.write(path, strToBytes);

        String read = Files.readAllLines(path).get(0);

        LOGGER.info(read);
    }

    public void readFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        String read = Files.readAllLines(path).get(0);

        LOGGER.info(read);
    }
}
