import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

public class CollectionExamples {

    private static final Logger LOGGER = Logger.getLogger( CollectionExamples.class.getName() );

    private static final Object[] objectArray = new Object[]{"aaa",9, new FileExamples()};
    private static final List<Object> list = Arrays.asList("sdfff", null, 20);
    private static final Map<Object, Object> map = new HashMap<Object,Object>(){{
        put("asdgh", "ppp");
        put(3, "ppp");
    }};



    public static void main(String[] args) throws IOException {
        LOGGER.info(objectArray.toString());
        LOGGER.info(list.toString());
        LOGGER.info(map.toString());
    }
}