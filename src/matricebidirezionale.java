/*
 * Build a matrix with 2 input args; the matrix must go from 100 to 0
 * the latest positions must have 0 values
 * */
public class matricebidirezionale {

	public static void main(String[] args) {
		
		//To assign first parameter to first variable to use in for instructions
		int v1 = Integer.parseInt(args[0]);
		//To assign second parameter to second variable to use in for instructions
		int v2 = Integer.parseInt(args[1]);
		
		//Matrix initialization 
		int matri[][] = new int[v1][v2] ;
		//Print variable values
		System.out.println(v1 +""+ v2);
		//To assign in an int variable a value to scale for matrix value assignment
		int scala = 100;
		
		//First cycle for row iterator compared with first args
		for (int idx = 0; idx < v1; idx ++) {
			//Second cycle for column iterator compared with second args
			for (int ydx = 0; ydx < v2; ydx ++) {
				//The value cannot be assigned if <= to zero 
				if (scala != 0 ) {
					//matrix assignment
					matri[idx][ydx] = scala;
					//work variable decrease
					scala--;
				}
				//Print current value
				System.out.print("\t" + matri[idx][ydx] + " ");
			}
			//Print a space
			System.out.println(" ");
		}
		
	}

}
